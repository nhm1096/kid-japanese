//
//  Extension.swift
//  Kid-Japanese
//
//  Created by Nguyễn Minh on 03/03/2019.
//  Copyright © 2019 Nguyễn Minh. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerCellWithIdentifier(identifier: String) {
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
}

extension UIButton {
    func applyRoundConner(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func applyRoundCircle() {
        applyRoundConner(self.frame.height / 2)
    }
    
}

extension UIView {
    func applyRoundedCorner(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func applyRoundedCorner() {
        applyRoundedCorner(radius: self.frame.height/2)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    class var blueAppColor: UIColor {
        return UIColor(hexString: "#7bcff1")
    }
    
    class var purpleAppColor: UIColor {
        return UIColor(hexString: "#c92276")
    }
    
    class var greenAppColor: UIColor {
        return UIColor(hexString: "#005334")
    }
    
}

extension UITableView {
    func registerCellWithNib(identifier: String) {
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
}

extension NSObject {
    class func name() -> String{
        let path = NSStringFromClass(self)
        return path.components(separatedBy: ".").last ?? ""
    }
}

extension UIStoryboard {
    class func initViewController() -> UIViewController? {
        let storyBoard = UIStoryboard(name: self.name(), bundle: nil)
        return storyBoard.instantiateInitialViewController()
    }
    
    class func Level() -> LevelViewController {
        let viewController = LevelViewController(nibName: LevelViewController.name(), bundle: nil)
        return viewController
    }
}

class OptionStoryboard: UIStoryboard {}
class LevelStoryboard: UIStoryboard {}
