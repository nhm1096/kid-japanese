//
//  SettingViewController.swift
//  Kid-Japanese
//
//  Created by Nguyễn Minh on 03/03/2019.
//  Copyright © 2019 Nguyễn Minh. All rights reserved.
//

import UIKit
import AVFoundation

class SettingViewController: UIViewController, MusicDelegate {
    
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var voiceSwitch: UISwitch!
    @IBOutlet weak var SoundSwitch: UISwitch!
    @IBOutlet weak var RandomSwitch: UISwitch!
    
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        settingView.applyRoundedCorner(radius: 10)
        
        loadUserSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadMusicState()
    }
    
    func loadUserSetting() {
        let defaults = UserDefaults.standard
        
        if (defaults.object(forKey: "voiceSwitch") != nil) {
            voiceSwitch.isOn = defaults.bool(forKey: "voiceSwitch")
        }
        if (defaults.object(forKey: "soundSwitch") != nil) {
            SoundSwitch.isOn = defaults.bool(forKey: "soundSwitch")
        }
        if (defaults.object(forKey: "randomSwitch") != nil) {
            RandomSwitch.isOn = defaults.bool(forKey: "randomSwitch")
        }
    }
    
    @IBAction func voiceState(_ sender: Any) {
        let defaults = UserDefaults.standard
        if voiceSwitch.isOn {
            defaults.set(true, forKey: "voiceSwitch")
        }
        else {
            defaults.set(false, forKey: "voiceSwitch")
        }
    }
    @IBAction func soundState(_ sender: Any) {
        let defaults = UserDefaults.standard
        if SoundSwitch.isOn {
            defaults.set(true, forKey: "soundSwitch")
        }
        else {
            defaults.set(false, forKey: "soundSwitch")
        }
    }
    @IBAction func randomState(_ sender: Any) {
        let defaults = UserDefaults.standard
        if RandomSwitch.isOn {
            defaults.set(true, forKey: "randomSwitch")
        }
        else {
            defaults.set(false, forKey: "randomSwitch")
        }
    }
    
    @IBAction func musicState(_ sender: Any) {
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "musicPlay") != nil) {
            let playing = defaults.bool(forKey: "musicPlay")
            
            if !playing {
                audioPlayer?.play()
                musicButton.setImage(UIImage(named: "audio-icon"), for: .normal)
            }
            else {
                audioPlayer?.pause()
                musicButton.setImage(UIImage(named: "audio-icon-disable"), for: .normal)
            }
            defaults.set(!playing, forKey: "musicPlay")
        }
    }
    
    func loadMusicState() {
        let defaults = UserDefaults.standard
        let playing = defaults.bool(forKey: "musicPlay")
        
        if playing {
            audioPlayer?.play()
            musicButton.setImage(UIImage(named: "audio-icon"), for: .normal)
        }
        else {
            audioPlayer?.pause()
            musicButton.setImage(UIImage(named: "audio-icon-disable"), for: .normal)
        }
    }
    
    @IBAction func backHome(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
