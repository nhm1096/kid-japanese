//
//  LevelViewController.swift
//  Kid-Japanese
//
//  Created by Nguyễn Minh on 04/03/2019.
//  Copyright © 2019 Nguyễn Minh. All rights reserved.
//

import UIKit

class LevelViewController: UIViewController {

    @IBOutlet weak var soundButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        soundButton.applyRoundCircle()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backHome(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
