//
//  ViewController.swift
//  Kid-Japanese
//
//  Created by Nguyễn Minh on 02/03/2019.
//  Copyright © 2019 Nguyễn Minh. All rights reserved.
//

import UIKit
import AVFoundation

protocol MusicDelegate {
    var audioPlayer: AVAudioPlayer? { get set }
    
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MusicDelegate {
    
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var levelCollectionView: UICollectionView!
    
    var audioPlayer: AVAudioPlayer?
    
    let level = ["Level 1", "Level 2", "Level 3", "Level 4", "Level 5", "Level 6", "Level 7", "Level 8"]
    let image = ["Image1", "Image2", "Image3", "Image4", "Image5", "Image6", "Image7", "Image8"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //play music
        playBackgroundMusic()
        
        // Do any additional setup after loading the view, typically from a nib.
        levelCollectionView.dataSource = self
        levelCollectionView.delegate = self
        levelCollectionView.registerCellWithIdentifier(identifier: LevelCollectionViewCell.name())
        levelCollectionView.backgroundColor = UIColor.clear
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadMusicState()
    }
    
    func playBackgroundMusic() {
        
        if let audioPlayer = audioPlayer, audioPlayer.isPlaying { audioPlayer.stop() }
        guard let soundURL = Bundle.main.url(forResource: "backgroundmusic", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            audioPlayer?.volume = 0.1
            audioPlayer?.numberOfLoops = -1
            audioPlayer?.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = levelCollectionView.dequeueReusableCell(withReuseIdentifier: LevelCollectionViewCell.name(), for: indexPath) as! LevelCollectionViewCell
            cell.setupCellData(level[indexPath.row], image[indexPath.row])
            return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        audioPlayer?.pause()
        let cell = levelCollectionView.cellForItem(at: indexPath)
        selectFadeOut(cell)
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LevelViewController") as! LevelViewController
        next.modalTransitionStyle = .crossDissolve
        self.present(next, animated: true, completion: nil)
        
        return
    }
    
    func selectFadeOut(_ cell: UICollectionViewCell?) {
        UIView.animate(withDuration: 0.5,
                       animations: {
                        //Fade-out
                        cell?.alpha = 0.5
        }) { (completed) in
            UIView.animate(withDuration: 0.5,
                           animations: {
                            //Fade-out
                            cell?.alpha = 1
            })
        }
    }

    @IBAction func musicState(_ sender: Any) {
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "musicPlay") != nil) {
            let playing = defaults.bool(forKey: "musicPlay")

            if !playing {
                audioPlayer?.play()
                musicButton.setImage(UIImage(named: "audio-icon"), for: .normal)
            }
            else {
                audioPlayer?.pause()
                musicButton.setImage(UIImage(named: "audio-icon-disable"), for: .normal)
            }
            defaults.set(!playing, forKey: "musicPlay")
        }
    }
    
    func loadMusicState() {
        let defaults = UserDefaults.standard
        let playing = defaults.bool(forKey: "musicPlay")
        
        if playing {
            audioPlayer?.play()
            musicButton.setImage(UIImage(named: "audio-icon"), for: .normal)
        }
        else {
            audioPlayer?.pause()
            musicButton.setImage(UIImage(named: "audio-icon-disable"), for: .normal)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SettingViewController
        {
            vc.audioPlayer = self.audioPlayer
        }
    }

}

