//
//  LevelCollectionViewCell.swift
//  Kid-Japanese
//
//  Created by Nguyễn Minh on 03/03/2019.
//  Copyright © 2019 Nguyễn Minh. All rights reserved.
//

import UIKit

class LevelCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var background: UIView!
    @IBOutlet weak var imageBackground: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
        
    }
    
    override func select(_ sender: Any?) {
        //
    }
    
    func setupCell() {
        self.background.applyRoundedCorner(radius: 20)
        self.background.backgroundColor = UIColor.greenAppColor
        self.imageBackground.roundCorners(corners: [.topLeft, .topRight], radius: 15)
    }
    
    func setupCellData(_ levelString: String, _ imageString: String) {
        self.imageView.image = UIImage.init(named: imageString)
        self.levelLabel.text = levelString
    }

}
